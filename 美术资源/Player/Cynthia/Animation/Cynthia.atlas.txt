
Cynthia.png
size: 512,128
format: RGBA8888
filter: Linear,Linear
repeat: none
书_书脊
  rotate: false
  xy: 271, 101
  size: 6, 25
  orig: 6, 25
  offset: 0, 0
  index: -1
书_书页
  rotate: false
  xy: 178, 2
  size: 18, 3
  orig: 18, 3
  offset: 0, 0
  index: -1
书_封面
  rotate: false
  xy: 285, 8
  size: 18, 25
  orig: 18, 25
  offset: 0, 0
  index: -1
前发
  rotate: true
  xy: 178, 7
  size: 54, 62
  orig: 54, 62
  offset: 0, 0
  index: -1
十字架
  rotate: true
  xy: 285, 2
  size: 4, 6
  orig: 4, 6
  offset: 0, 0
  index: -1
右手_1
  rotate: true
  xy: 305, 77
  size: 9, 8
  orig: 9, 8
  offset: 0, 0
  index: -1
右腿_上
  rotate: false
  xy: 268, 35
  size: 16, 26
  orig: 18, 26
  offset: 0, 0
  index: -1
右腿_下
  rotate: true
  xy: 123, 52
  size: 8, 18
  orig: 8, 18
  offset: 0, 0
  index: -1
嘴
  rotate: false
  xy: 293, 2
  size: 6, 4
  orig: 6, 4
  offset: 0, 0
  index: -1
头发后面-_2
  rotate: true
  xy: 2, 2
  size: 24, 59
  orig: 24, 59
  offset: 0, 0
  index: -1
头发后面_1
  rotate: false
  xy: 241, 86
  size: 28, 40
  orig: 28, 40
  offset: 0, 0
  index: -1
头发后面_3
  rotate: false
  xy: 2, 28
  size: 80, 98
  orig: 80, 98
  offset: 0, 0
  index: -1
左手_1
  rotate: false
  xy: 317, 118
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
左腿_上
  rotate: false
  xy: 269, 7
  size: 14, 26
  orig: 14, 26
  offset: 0, 0
  index: -1
左腿_下
  rotate: false
  xy: 284, 69
  size: 8, 17
  orig: 8, 17
  offset: 0, 0
  index: -1
帽子
  rotate: true
  xy: 123, 62
  size: 64, 53
  orig: 64, 53
  offset: 0, 0
  index: -1
球
  rotate: true
  xy: 178, 63
  size: 63, 61
  orig: 63, 61
  offset: 0, 0
  index: -1
眉毛
  rotate: false
  xy: 198, 3
  size: 19, 2
  orig: 19, 2
  offset: 0, 0
  index: -1
眼珠_右
  rotate: false
  xy: 307, 117
  size: 8, 9
  orig: 8, 9
  offset: 0, 0
  index: -1
眼珠_左
  rotate: true
  xy: 307, 109
  size: 6, 9
  orig: 6, 9
  offset: 0, 0
  index: -1
眼白
  rotate: true
  xy: 299, 97
  size: 10, 9
  orig: 27, 9
  offset: 0, 0
  index: -1
眼镜
  rotate: false
  xy: 284, 88
  size: 13, 17
  orig: 13, 17
  offset: 0, 0
  index: -1
睫毛_右
  rotate: true
  xy: 295, 36
  size: 13, 8
  orig: 13, 8
  offset: 0, 0
  index: -1
睫毛_左
  rotate: false
  xy: 299, 88
  size: 9, 7
  orig: 9, 7
  offset: 0, 0
  index: -1
胸
  rotate: true
  xy: 286, 35
  size: 14, 7
  orig: 14, 7
  offset: 0, 0
  index: -1
脖子
  rotate: false
  xy: 310, 101
  size: 6, 6
  orig: 6, 6
  offset: 0, 0
  index: -1
脸
  rotate: false
  xy: 96, 2
  size: 45, 48
  orig: 45, 48
  offset: 0, 0
  index: -1
蝴蝶结_
  rotate: true
  xy: 242, 32
  size: 29, 24
  orig: 29, 24
  offset: 0, 0
  index: -1
蝴蝶结_头_1
  rotate: true
  xy: 241, 63
  size: 21, 28
  orig: 21, 28
  offset: 0, 0
  index: -1
蝴蝶结_头_2
  rotate: false
  xy: 279, 107
  size: 12, 19
  orig: 12, 19
  offset: 0, 0
  index: -1
蝴蝶结_头_3
  rotate: false
  xy: 271, 76
  size: 11, 23
  orig: 11, 23
  offset: 0, 0
  index: -1
蝴蝶结_头_4
  rotate: true
  xy: 63, 2
  size: 24, 31
  orig: 24, 31
  offset: 0, 0
  index: -1
蝴蝶结_胸
  rotate: true
  xy: 286, 51
  size: 16, 12
  orig: 16, 12
  offset: 0, 0
  index: -1
袖子_右_上
  rotate: false
  xy: 293, 109
  size: 12, 17
  orig: 12, 17
  offset: 0, 0
  index: -1
袖子_右_下
  rotate: false
  xy: 271, 63
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
袖子_左_上
  rotate: false
  xy: 84, 33
  size: 10, 17
  orig: 10, 17
  offset: 0, 0
  index: -1
袖子_左_下
  rotate: false
  xy: 300, 57
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
裙子后面
  rotate: true
  xy: 143, 4
  size: 56, 33
  orig: 56, 33
  offset: 0, 0
  index: -1
裙摆_
  rotate: true
  xy: 84, 52
  size: 74, 37
  orig: 74, 37
  offset: 0, 0
  index: -1
身体
  rotate: true
  xy: 242, 4
  size: 26, 25
  orig: 28, 25
  offset: 1, 0
  index: -1
领子
  rotate: true
  xy: 294, 71
  size: 15, 9
  orig: 15, 9
  offset: 0, 0
  index: -1
高光
  rotate: false
  xy: 84, 28
  size: 3, 3
  orig: 3, 3
  offset: 0, 0
  index: -1
