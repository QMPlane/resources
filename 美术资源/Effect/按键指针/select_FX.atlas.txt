
select_FX.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
_0000_select_glow
  rotate: false
  xy: 2, 1084
  size: 256, 256
  orig: 256, 256
  offset: 0, 0
  index: -1
_0001_select_point
  rotate: false
  xy: 1462, 1089
  size: 256, 256
  orig: 256, 256
  offset: 0, 0
  index: -1
_0002_select_shadow
  rotate: false
  xy: 1720, 1089
  size: 256, 256
  orig: 256, 256
  offset: 0, 0
  index: -1
title_01
  rotate: true
  xy: 2, 1342
  size: 3, 1458
  orig: 3, 1458
  offset: 0, 0
  index: -1
title_02
  rotate: true
  xy: 2, 2
  size: 1080, 1920
  orig: 1080, 1920
  offset: 0, 0
  index: -1
